﻿#include <iostream>



class Animal
{
private: 
	

public:

	Animal() 
	{};

	virtual void Voice()
	{
		std::cout << "" << std::endl;
	}

};



class Cat : public Animal
{
public:
	void Voice() 
	{
		std::cout << "Myoo" << "\n";
	}
};

class Dog : public Animal
{
public:
	void Voice()
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Lion : public Animal
{
public:
	std::string sound = "Roaf";
	void Voice()
	{
		std::cout << "Roaf!" << std::endl;
	}
};

int main()
{
	setlocale(LC_ALL, "rus");


	Animal* animal[3];

	Animal* cat = new Cat();
	Animal* dog = new Dog();
	Animal* lion = new Lion();
	
	animal[0] = cat;
	animal[1] = dog;
	animal[2] = lion;


	for (auto &element : animal) 
	{
		element->Voice();
	}

	*animal = nullptr;
	//delete[] animal;
	
	cat = nullptr;
	delete cat;
	dog = nullptr;
	delete dog;
	lion = nullptr;
	delete lion;
	
	

	return 0;
}
